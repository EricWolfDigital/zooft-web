import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import firebase from 'firebase';

Vue.config.productionTip = false;
Vue.use(firebase); //sets up firebase object

// TODO: Replace the following with your app's Firebase project configuration
var firebaseConfig = {
  apiKey: 'AIzaSyBCx1ehamsXQYlvBOtkbl_OR2Id-9OwfAE',
  authDomain: 'zooft-2bf31.firebaseapp.com',
  databaseURL: 'https://zooft-2bf31.firebaseio.com',
  projectId: 'zooft-2bf31',
  storageBucket: 'zooft-2bf31.appspot.com',
  messagingSenderId: '893187818679',
  appId: '1:893187818679:web:57073590fdfe2cb0d4b932',
  measurementId: 'G-6Y7Y9KP3SB'
};

// Initialize Firebase
export const app = firebase.initializeApp(firebaseConfig);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
