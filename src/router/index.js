import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';
import Terms from '@/views/Terms.vue';
import Login from '@/views/Login.vue';
import Confidentiality from '@/views/Confidentiality.vue';
import Business from '@/views/Business.vue';
import Edit from '@/views/Edit.vue';
import Menu from '@/views/Menu.vue';
import EditMenu from '@/views/EditMenu.vue';
import DeleteMenu from '@/views/DeleteMenu.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/termeni-conditii',
    name: 'terms',
    component: Terms
    // // route level code-splitting
    // // this generates a separate chunk (about.[hash].js) for this route
    // // which is lazy-loaded when the route is visited.
    // component: () =>
    //   import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: '/politica-de-confidentialitate',
    name: 'confidentiality',
    component: Confidentiality
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/:businessname/profil',
    name: 'business',
    component: Business
  },
  {
    path: '/:businessname/edit',
    name: 'edit',
    component: Edit
  },
  {
    path: '/:businessname/delete',
    name: 'delete',
    component: DeleteMenu
  },
  {
    path: '/:businessname/add',
    name: 'add',
    component: EditMenu
  },
  {
    path: '/:id',
    name: 'menu',
    component: Menu
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
  // eslint-disable-next-line no-unused-vars
  scrollBehavior(to, from, savedPosition) {
    window.scroll({
      top: to,
      left: 0,
      behavior: 'auto'
    });
  }
});

export default router;
