import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    id: '',
    logged: 'false',
    name: ''
  },
  mutations: {
    updateid(state, payload) {
      state.id = payload;
      // console.log('updated id to ', payload);
    },
    updatename(state, payload) {
      state.name = payload;
    },
    changelogged(state, payload) {
      state.logged = payload;
      console.log('changed logged state');
    }
  },
  actions: {},
  modules: {}
});
